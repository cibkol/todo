export default function InputTitle() {
    return (
        <div className="input content__new-todo_input-title">
          <input className="content__new-todo_input-title_input"type="text" placeholder="Название задачи"/>
        </div>
    )
}
