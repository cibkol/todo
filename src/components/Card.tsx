import { ICard } from "../type";

export default function Card({ title, about, priority, data, isComplete, onComplete, index }:ICard) {
    return (
        <div className="card content__todo-views_cards_card">
            <div className="content__todo-views_cards_card_header">
                <h1 className="content__todo-views_cards_card_header_title">{title}</h1>
                <span className="content__todo-views_cards_card_header_priority">Приоритет {priority}</span>
                <span className="content__todo-views_cards_card_header_data">{data}</span>
            </div>
            <div className="content__todo-views_cards_card_content">
                <p className="content__todo-views_cards_card_content_about">
                    {about}
                </p>
            </div>
            <div className="content__todo-views_cards_card_footer">
                <button onClick={() => onComplete(index)} className={`content__todo-views_cards_card_footer_btn btn`}>{`${isComplete ? 'Выполненно' : 'Выполнить'}`}</button>
            </div>
        </div>
    )
}
