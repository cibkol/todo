import Check from '../assets/svg/check.svg';
import Arrow from '../assets/svg/arrow.svg';

export default function NewTodoBottom({ onClick }: any ) {
    return (
        <div className="content__new-todo_bottom">
          <div className="input content__new-todo_input-data">
            <input className="content__new-todo_input-data_input" type="text" placeholder="Дедлайн" />
          </div>

          <div className="content__new-todo_bottom_priority">
            <span className="content__new-todo_bottom_priority_placeholder">Приоритет</span>
            <span className="content__new-todo_bottom_priority_active-priority">Приоритет 2</span>
            <img className="content__new-todo_bottom_priority_arrow" src={Arrow} alt="arrow" />
            <div className="popup hidden content__new-todo_bottom_priority_popup">
              <span className="content__new-todo_bottom_priority_popup_stroke">Приоритет 1</span>
              <span className="active content__new-todo_bottom_priority_popup_stroke">
                <img src={Check} alt="check" />

                Приоритет 2</span>
              <span className="content__new-todo_bottom_priority_popup_stroke">Приоритет 3</span>
              <span className="content__new-todo_bottom_priority_popup_stroke">Приоритет 4</span>
            </div>
          </div>

          <div className="content__new-todo_bottom_accept">
            <button onClick={onClick} className="btn content__new-todo_bottom_accept_btn"> Создать </button>
          </div>
        </div>
    )
}
