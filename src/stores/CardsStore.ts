import { makeAutoObservable } from "mobx";
import { ICard, TCard } from "../type";

class todoCardArray {
    cards: TCard = [
        {
            title: '1',
            about: '1',
            data: '11.11.1111',
            priority: 1,
            isComplete: false
        },
        {
            title: '2',
            about: '2',
            data: '22.22.2222',
            priority: 2,
            isComplete: true
        },

    ];

    error = (e: any, message: string) => {
        console.error(e);
        alert(message);
    }

    onComplete = (index: number | undefined) => {
        typeof index === 'number' ? 
        this.cards[index].isComplete = !this.cards[index].isComplete :
        this.error(index, 'Ошибка выполнения/обнуления todo')
    };

    addNewCard = (obj: ICard) => {
        this.cards.push(obj);
    };

    constructor() {
        makeAutoObservable(this)
    }
}

export default new todoCardArray();