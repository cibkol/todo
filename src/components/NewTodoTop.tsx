import { NavLink } from 'react-router-dom';
import GreenCircle from '../assets/svg/green-circle.svg';

export default function NewTodoTop() {
    return (
        <div className="content__new-todo_top">
          <span className="content__new-todo_top_left">
            <img src={GreenCircle} alt="green circle" />
            <h1 className="content__new-todo_top_left_title">Добавить задачу</h1>
          </span>
          <NavLink to="/" className="btn content__new-todo_top_cancel">Отмена</NavLink>
        </div>
    )
}
