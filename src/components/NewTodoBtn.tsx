import { NavLink } from "react-router-dom";

export default function NewTodoBtn() {
    return (
        <NavLink to="/new-todo" className="btn content__btn-new-todo">Добавить задачу</NavLink>
    )
}
