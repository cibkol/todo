import GreenCircle from '../assets/svg/green-circle.svg';

type ISort = {
    arr: Array<string>;
    active: number;
    sortSelect: any;
}

export default function Sort({ arr, active, sortSelect }: ISort) {
    return (
        <div className="content__todo-views_sort">
            {arr.map((e, index) => (
                <span key={`${e}_${index}`} onClick={() =>(sortSelect(index))} className={`content__todo-views_sort_span ${active === index ? "active" : ""}`}>{active === index ? <img src={GreenCircle} alt="green circle" /> : ""}{e}</span>
            ))}
        </div>
    )
}
