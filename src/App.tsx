import './index.scss';

import { Route } from 'react-router';

import Header from './components/Header';

import Home from './pages/Home';
import NewTodoPage from './pages/NewTodoPage';

function App() {
  return (
    <div className="app">
      <div className="wrapper">
        <Header />

        <div className="content">
          <Route path="/" component={Home} exact/>
          <Route path="/new-todo" component={NewTodoPage} exact/>
        </div>
        
      </div>
    </div>
  );
}

export default App;
