import { makeAutoObservable } from "mobx";
import { TSort } from "../type";

class sortArray {
    sortArr: TSort = [
        'Все',
        'Текущие задачи',
        'Завершенные'
    ];

    isActive = 0;

    sortSelect = (index: number) => {
        this.isActive = index;
    };

    constructor() {
        makeAutoObservable(this)
    };
}

export default new sortArray();