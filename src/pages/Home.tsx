
import { observer } from 'mobx-react';

import CardsStore from '../stores/CardsStore';
import SortStore from '../stores/SortStore';

import NewTodoBtn from '../components/NewTodoBtn';
import Sort from '../components/Sort';
import Card from '../components/Card';

const cards = CardsStore;

const arr = SortStore;

export const Home = observer(() => {


    return (
        <>
            
            <NewTodoBtn />

            <div className="content__todo-views">
                <Sort arr={arr.sortArr} active={arr.isActive} sortSelect={arr.sortSelect}/>
                <div className="content__todo-views_cards">
                    {arr.isActive === 0 ? cards.cards.map((card,index) => (
                        <Card key={`${card.title}_${index}`} title={card.title} index={index} data={card.data} about={card.about} priority={card.priority} isComplete={card.isComplete} onComplete={cards.onComplete} />
                    )) : arr.isActive === 1 ? cards.cards.map((card,index) => {
                        if (!card.isComplete) {
                            return(<Card key={`${card.title}_${index}`} index={index} title={card.title} data={card.data} about={card.about} priority={card.priority} isComplete={card.isComplete} onComplete={cards.onComplete} />)
                        }
                    }) : arr.isActive === 2 ? cards.cards.map((card,index) => {
                        if (card.isComplete) {
                            return(<Card key={`${card.title}_${index}`} index={index} title={card.title} data={card.data} about={card.about} priority={card.priority} isComplete={card.isComplete} onComplete={cards.onComplete} />)
                        }
                    }) : ""   
                    }

                </div>
            </div>
        </>
    )
}
)

export default Home;