export type TCard = Array<{
    title: string;
    about: string;
    priority: number;
    data: string;
    isComplete: boolean;
}>

export type TSort = string[]


export type ICard = {
    title: string;
    about: string;
    priority: number;
    index?: number;
    data: string;
    isComplete: boolean;
    onComplete: any;
}
