export default function Header() {
    return (
        <header className="header">
            <h1 className="header__title">TODOAPP</h1>
        </header>
    )
}
