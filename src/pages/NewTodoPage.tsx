import { observer } from 'mobx-react';

import CardsStore from '../stores/CardsStore';

import InputAbout from '../components/InputAbout';
import InputTitle from '../components/InputTitle';
import NewTodoBottom from '../components/NewTodoBottom';
import NewTodoTop from '../components/NewTodoTop';


const cards = CardsStore;

const NewTodoPage = observer(() => {
    const onClickPush = () => {
      cards.addNewCard({
        title: 'New Todo',
        about: 'New Todo',
        priority: 3,
        data: '33.33.3333',
        isComplete: false,
        onComplete: cards.onComplete
      })
    };

    return (
      <div className="content__new-todo">
        <NewTodoTop />
        <InputTitle />
        <InputAbout />
        <NewTodoBottom onClick={onClickPush} />
        

      </div>
    )
})

export default NewTodoPage;